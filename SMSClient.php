<?php
//
// PHP SMS Client for Symbio Networks
// Version 0.1
// Nicholas Terrell <nicholas.terrell@mynetfone.com.au> - 2015-01-21

class SMSClient extends s3client {

    protected $url = 'https://sms.symbionetworks.com/api/v1/';

    // send
    public function send($data){
        $response = $this->post("/send", $data);
        return $response;
    }

    public function getSend($condition = array()){
        $response = $this->post("/send/get", $condition);
        return $response;
    }

    // receive
    public function getReceive($condition = array()){
        $response = $this->post("/receive/get", $condition);
        return $response;
    }

    // account
    public function auth(){
        $response = $this->get("/account/auth");
        return $response;
    }

    public function getFromNumbers(){
        $response = $this->get("/account/numbers");
        return $response;
    }

    public function getPostbackUrl(){
        $response = $this->get("/account/postback");
        return $response;
    }

    public function updatePostbackUrl($sms_postback = false, $report_postback = false){
        $postdata = [];
        if($sms_postback !== false){ $postdata['sms_postback'] = $sms_postback; }
        if($report_postback !== false){ $postdata['report_postback'] = $report_postback; }
        $response = $this->post("/account/postback", $postdata);
        return $response;
    }

}

class s3client {

    protected $client;
    protected $key;
    protected $url;

    function __construct($key = null){
        $this->key = $key;
    }

    protected function buildUrl($endpoint){
        return $this->url.$endpoint;
    }

    protected function buildHeaders($headers, $output = 'array'){
        $headers['Authorization'] = "apikey {$this->key}";
        if($output == 'string'){
            $headerdata = "";
            foreach($headers as $key => $val){
                $headerdata .= "$key: $val \r\n";
            }
            return $headerdata;
        }
        return $headers;
    }

    public function get($endpoint){
        $url = $this->buildUrl($endpoint);
        $json = json_encode($data);
        $headers = $this->buildHeaders(['Content-Type' => 'application/json'], 'string');

        $result = file_get_contents($url, null, stream_context_create(array(
                    'http' => array(
                        'method' => 'GET',
                        'header' => $headers
                    ),
                )));

        $resp = Response::loadBody($result);
        if(!$resp instanceof Response){
            throw new SMSClientException("Failed to load response body");
        }
        return $resp;
    }

    public function post($endpoint, array $data){
        $url = $this->buildUrl($endpoint);
        $json = json_encode($data);
        $headers = $this->buildHeaders(['Content-Type' => 'application/json', 'Content-Length' => strlen($json)], 'string');

        $result = file_get_contents($url, null, stream_context_create(array(
                    'http' => array(
                        'method' => 'POST',
                        'header' => $headers,
                        'content' => $json,
                    ),
                )));

        $resp = Response::loadBody($result);
        if(!$resp instanceof Response){
            throw new SMSClientException("Failed to load response body");
        }
        return $resp;
    }

}

class SMSClientException extends Exception { }

class Response {

    protected $status;
    protected $message;
    protected $data;
    protected $error_code;

    public function  __construct($status = null, $message = null, $data = null, $error_code = null){
        $this->status = $status;
        $this->message = $message;
        $this->data = $data;
        $this->error_code = $error_code;
    }

    public static function loadBody($body){
        if(empty($body)){ return false; }
        $response = new self;
        if(($json = json_decode($body, true)) === NULL){
            return false;
        }

        if(isset($json['status'])){
            $response->setStatus($json['status']);
        }
        if(isset($json['message'])){
            $response->setMessage($json['message']);
        }
        if(isset($json['data'])){
            $response->setData($json['data']);
        }
        if(isset($json['error_code'])){
            $response->setErrorCode($json['error_code']);
        }
        return $response;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setStatus($status){
        $this->status = $status;
        return $this;
    }

    public function getMessage(){
        return $this->message;
    }

    public function setMessage($message){
        $this->message = $message;
        return $this;
    }

    public function getData($return_key = false){
        if($return_key !== false){
            if(is_array($this->data)&&array_key_exists($return_key, $this->data)){
                return $this->data[$return_key];
            }
            return false;
        }
        return $this->data;
    }

    public function setData($data){
        $this->data = $data;
        return $this;
    }

    public function getErrorCode(){
        return $this->error_code;
    }

    public function setErrorCode($error_code){
        $this->error_code = $error_code;
        return $this;
    }

    public function isSuccess(){
        if(!empty($this->status)&&$this->status == "success"){
            return true;
        }
        return false;
    }


}